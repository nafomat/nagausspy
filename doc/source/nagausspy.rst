nagausspy package
=================

Submodules
----------

nagausspy.analysis module
-------------------------

.. automodule:: nagausspy.analysis
   :members:
   :undoc-members:
   :show-inheritance:

nagausspy.frequency module
--------------------------

.. automodule:: nagausspy.frequency
   :members:
   :undoc-members:
   :show-inheritance:

nagausspy.geometry module
-------------------------

.. automodule:: nagausspy.geometry
   :members:
   :undoc-members:
   :show-inheritance:

nagausspy.nmr module
--------------------

.. automodule:: nagausspy.nmr
   :members:
   :undoc-members:
   :show-inheritance:

nagausspy.reader module
-----------------------

.. automodule:: nagausspy.reader
   :members:
   :undoc-members:
   :show-inheritance:

nagausspy.templates module
--------------------------

.. automodule:: nagausspy.templates
   :members:
   :undoc-members:
   :show-inheritance:

nagausspy.widening\_functions module
------------------------------------

.. automodule:: nagausspy.widening_functions
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nagausspy
   :members:
   :undoc-members:
   :show-inheritance:
