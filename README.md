# NaGausspy
[![coverage report](https://gitlab.com/nafomat/nagausspy/badges/master/coverage.svg)](https://gitlab.com/nafomat/nagausspy/-/commits/master)
[![pipeline status](https://gitlab.com/nafomat/nagausspy/badges/master/pipeline.svg)](https://gitlab.com/nafomat/nagausspy/-/commits/master)
[![Latest Release](https://gitlab.com/nafomat/nagausspy/-/badges/release.svg)](https://gitlab.com/nafomat/nagausspy/-/releases)


## Installation
The easiest way of installation is using Pypi with pip:
`pip install nagausspy`

## Documentation
The documentation is available at [Gitlab Pages](https://nafomat.gitlab.io/nagausspy/)

## TODO

- Add more tests
- Improve optimization step reading
- Simpler and clear documentation